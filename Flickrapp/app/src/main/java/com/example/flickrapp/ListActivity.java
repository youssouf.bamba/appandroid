package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.RequestQueue;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class ListActivity extends AppCompatActivity {
    String url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //recuperation de la preference qui est par defaut à tree
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String b = prefs.getString("Image","");
        if (b.equals("cat")){
            url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=cats&format=json";
        }
        MyAdaptater adaptater = new MyAdaptater();
        ListView l = (ListView) findViewById(R.id.list);
        l.setAdapter(adaptater);

        AsyncFlickrJSONDataForList asyncFlickrJSONDataForList = new AsyncFlickrJSONDataForList(adaptater);
        asyncFlickrJSONDataForList.execute(url);


    }

    //classe adapter qui permet de derouler la liste des images envoyées et/ou les affichées
    public class MyAdaptater extends BaseAdapter {

        Vector<String> vector = new Vector<String>();

        public void dd(String url){
            vector.add(url);
        }

        @Override
        public int getCount() {
            return vector.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater inflater = LayoutInflater.from(ListActivity.this);
            /*
            View inflated = inflater.inflate(R.layout.textviewlayout, null);
            TextView tv =(TextView) inflated.findViewById(R.id.textview);
            tv.setText(vector.get(i));
            return inflated;
            */
            View inflated = inflater.inflate(R.layout.bitmaplayout, null);


            RequestQueue queue = MySingleton.getInstance(viewGroup.getContext()).getRequestQueue();
            ImageRequest imagereq=new ImageRequest(vector.get(i), new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        ImageView imag= (ImageView) inflated.findViewById(R.id.imageview2);
                        imag.setImageBitmap(response);
                    }
                    },0,0, ImageView.ScaleType.CENTER_CROP,Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("er",error.toString());
                            error.printStackTrace();
                        }
            });

            queue.add(imagereq);

            return inflated;


        }


    }


    // A partir d'un url cette classe recupère toutes les images
    // doInBackground récupère les données de l'url donnée lors de l'appel de la methode execute
    //onPostExecute qui remplie sequentiellement l'attribut MyAdapter par les url des images
    public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {
        MyAdaptater adaptater;

        public AsyncFlickrJSONDataForList(MyAdaptater adapter){
            this.adaptater = adapter;
        }

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s = readStream(in);
                    Log.i("FLIKR", s);
                    JSONObject json = new JSONObject((s.subSequence(("jsonFlickrFeed(").length(),s.length()-1)).toString());
                    return json;
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }


        protected void onPostExecute(JSONObject result){
            try {
                JSONArray json = result.getJSONArray("items");
                String url;
                for (int i = 0;i<json.length();++i){
                    url = json.getJSONObject(i).getJSONObject("media").getString("m");
                    adaptater.dd(url);
                    adaptater.notifyDataSetChanged();
                    Log.i("AsyncFlickrJSONDataForList","ajout d'un adaptater URL" + url);
                };

            } catch (JSONException err){
                Log.d("Error",err.toString());
            }


        }
    }
}