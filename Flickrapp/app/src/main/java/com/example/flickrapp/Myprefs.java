package com.example.flickrapp;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Myprefs extends PreferenceActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //on charge le fhichier xml des preferences
        addPreferencesFromResource(R.xml.preferences);
    }

}
