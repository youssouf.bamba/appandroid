package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Activity extends AppCompatActivity {

    String url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json";
    ImageView iv;
    Button image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        //bouton get pour charger l'image
        image = (Button)findViewById(R.id.get);
        image.setOnClickListener( new GetImageOnClickListener());

        //le view qui contient l'image
        iv = (ImageView) findViewById(R.id.image);

        // ListActivity : affiche soit des photos d'arbres ou de chat en fonction du choix
        Button listActivity = (Button) findViewById(R.id.button_ListActivity);
        listActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(Activity.this,ListActivity.class);
                startActivity(i);

            }
        });

        //Les préferences : cat or tree
        View button = findViewById(R.id.prefs);
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go_to_prefs = new Intent(getApplicationContext(), Myprefs.class);
                startActivity(go_to_prefs);

            }
        });


    }


    // A partir d'un url cette classe recupère l'image correspondante
    // doInBackground récupère les données de l'url donnée lors de l'appel de la methode execute
    //onPostExecute affiche image correspondante
    public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject>{

        private String readStream(InputStream is) throws IOException{
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {

            URL url = null;
            try {

                url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    Log.i("mess",url.toString());
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    String s = readStream(in);
                    try {
                        JSONObject jsonObject = new JSONObject((s.subSequence(("jsonFlickrFeed(").length(),s.length()-1)).toString());
                        return jsonObject;
                    } catch (JSONException err){
                        Log.d("Error",err.toString());
                    }
                }catch (Exception e){
                    Log.d("Error",e.toString());
                }
                finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;


        }


        protected void onPostExecute(JSONObject result){
            Log.i("TAG",result.toString());
            try {
                String url_image = result.getJSONArray("items").getJSONObject(1).getJSONObject("media").getString("m");
                //Log.i("IMG", result.getJSONArray("items").getJSONObject(1).getString("link"));
                AsyncBitmapDownloader asyncBitmapDownloader = new AsyncBitmapDownloader();
                asyncBitmapDownloader.execute(url_image);
                Log.i("TEST",url_image);
            } catch (JSONException err){
                Log.d("Error",err.toString());
            }

        }
    }

    public class GetImageOnClickListener implements View.OnClickListener {
        String url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json";
        @Override
        public void onClick(View view) {
            AsyncFlickrJSONData asyncFlickrJSONData = new AsyncFlickrJSONData();
            asyncFlickrJSONData.execute(url);
        }
    }

    public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {
        /**
         * @param strings
         * @deprecated
         */
        @Override
        protected Bitmap doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Bitmap bm = BitmapFactory.decodeStream(in);
                    Log.i("Test bm",""+(bm==null));
                    return bm;
                }
                finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
        protected void onPostExecute (Bitmap result){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("Test on post",""+(result==null));
                    iv.setImageBitmap(result);
                }
            });
        }

    }
}